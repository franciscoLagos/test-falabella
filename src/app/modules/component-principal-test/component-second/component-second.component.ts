import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { infoCustomer } from '../../../interfaces/info-customer.interface';
import swal from 'sweetalert2';
import { CustomerService } from '../../../services/customer/customer.service';

@Component({
  selector: 'app-component-second',
  templateUrl: './component-second.component.html',
  styleUrls: ['./component-second.component.scss']
})
export class ComponentSecondComponent implements OnInit {
  @Input() dataOfCustomer: infoCustomer;
  formRenta: FormGroup;
  info: any;
  
  constructor(fb: FormBuilder,
    private router: Router,
    private customerService:CustomerService) {
    this.formRenta =fb.group({
      renta: ['']
    })
   
    
   }
   

  ngOnInit() {
    this.info=this.customerService.getCustomer();
    
  }
  
  //si el formulario esta ok
  formIsOk(){
    if(this.formRenta.value.renta==='' ){
      return true;
    }
    else{
      return false;
    }
  }

  sendRequestCustomer(){
    this.info.renta=(this.formRenta.get('renta').value).toString();
    
    this.customerService.insertInformacionCustomer(this.info).subscribe((result) => {
      
      swal.fire({
        title: 'Informacion almacenada',
        text: 'Se ha guardado con exito la información del cliente',
        icon: 'success',
        confirmButtonColor: '#28892B',
        confirmButtonText: 'Cerrar'
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this.router.navigate(['./']);
        } else if (result.isDenied) {
          this.router.navigate(['./']);
        }
      })
      
      
    }, (err) => {
     
      swal.fire({
        title: 'Error al guardar la información',
        text: 'Error tipo: ' + err.status + ', Reintente más tarde',
        icon: 'error',
        confirmButtonColor: '#28892B',
        confirmButtonText: 'Cerrar'
      });
    });
    
  }

 

}
