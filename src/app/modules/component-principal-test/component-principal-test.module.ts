import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule, MatStepperModule, MatPaginatorModule } from '@angular/material';

import { TokenInterceptor } from 'src/app/interceptors/token.interceptor';
import { AppErrorHandler } from 'src/app/app-error.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentPrincipalTestComponent } from './component-principal-test.component';
import { ComponentOneComponent } from './component-one/component-one.component';
import { ComponentSecondComponent } from './component-second/component-second.component';


@NgModule({
  imports: [CommonModule, RouterModule, SharedModule, MatDialogModule, MatStepperModule, MatPaginatorModule],
  declarations: [
    ComponentPrincipalTestComponent,
    ComponentOneComponent,
    ComponentSecondComponent,
   
    
  ],
  
  providers: [
    { provide: ErrorHandler, useClass: AppErrorHandler },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] }
  ],
  entryComponents: []
})
export class ComponentPrincipalTestModule { }
