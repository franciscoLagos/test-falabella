import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import { LoaderComponent } from './loader.component';

import { LoaderService } from './services/loader.service';

@NgModule({
  imports: [ CommonModule],
  declarations: [LoaderComponent],
  providers: [LoaderService],
  exports: [LoaderComponent]
})
export class LoaderModule {}
