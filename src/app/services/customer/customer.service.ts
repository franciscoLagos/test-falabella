import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { infoCustomer } from '../../interfaces/info-customer.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  public customer: infoCustomer;
  constructor(private http: HttpClient) { }
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  apiURL= 'http://localhost:3000/account';
  
  

  setterCustomer(customer: infoCustomer){
    this.customer=customer;
  }

  getCustomer(){
    return this.customer;

  }
  
  //servicio post para enviar info cliente al back
  insertInformacionCustomer(dataCustomer: infoCustomer){
    
    return this.http.post<any>(`${environment.apiConnect.crearInfoCliente}`, JSON.stringify(dataCustomer))
    .pipe(
      retry(1)
    
    )
      
  }

  
}
