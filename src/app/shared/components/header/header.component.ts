import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  sidebarIsOpen = true;

  constructor() {}

  ngOnInit() {}

  toggleSideBar() {
    this.sidebarIsOpen = !this.sidebarIsOpen;
    this.toggleSideBarForMe.emit();
  }

  openModal() {
    /* this.modal.open(ModalComponent); */
  }
}
