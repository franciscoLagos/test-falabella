import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTooltipModule,
  MatSidenavModule,
  MatDividerModule,
  MatToolbarModule,
  MatMenuModule,
  MatGridListModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDatepickerModule,
  MatNativeDateModule,
  MAT_DATE_LOCALE,
  MatSlideToggleModule,
  MatExpansionModule,
  MatTabsModule
} from '@angular/material';



import { RutDirective } from './directives';
import {
  CapitalizePipe,
  DatexPipe,
  OrderByPipe,
  SafePipe
} from './pipes';
import { HeaderComponent } from './components/header/header.component';

import { RouterModule } from '@angular/router';
import { SeparadorMilesPipe } from './pipes/separador-miles/separador-miles.pipe';
import { LoaderModule } from './components/loader/loader.module';
import { RutFormatPipe } from './pipes/rut-format/rut-format.pipe';
import { Ng2Rut } from 'ng2-rut';
import { AlertModule } from './components/alert/alert.module';
@NgModule({
  imports: [
    
    CommonModule,
    FlexLayoutModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatDividerModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatGridListModule,
    RouterModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    AlertModule,
    MatExpansionModule,
    MatTabsModule
  ],
  declarations: [
    CapitalizePipe,
    DatexPipe,
    OrderByPipe,
    RutDirective,
    RutFormatPipe,
    SafePipe,
    HeaderComponent,
    SeparadorMilesPipe
  ],
  exports: [
    
    CapitalizePipe,
    CommonModule,
    DatexPipe,
    FlexLayoutModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTooltipModule,
    OrderByPipe,
    ReactiveFormsModule,
    RutDirective,
    RutFormatPipe,
    SafePipe,
    HeaderComponent,
    MatSidenavModule,
    MatDividerModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatGridListModule,
    RouterModule,
    SeparadorMilesPipe,
    LoaderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    Ng2Rut,
    AlertModule,
    MatExpansionModule,
    MatTabsModule
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
    { provide: MAT_DATE_LOCALE, useValue: 'es-CL' }
  ]
})
export class SharedModule {}
