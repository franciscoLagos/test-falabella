import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { infoCustomer } from '../../../interfaces/info-customer.interface';
import { CustomerService } from '../../../services/customer/customer.service';

@Component({
  selector: 'app-component-one',
  templateUrl: './component-one.component.html',
  styleUrls: ['./component-one.component.scss']
})
export class ComponentOneComponent implements OnInit {
  
 
  public customer: infoCustomer = {
    rut: '',
    celular: '',
    correo: ''
  }
  buttonDisabled= true;
  formDateCustomer: FormGroup;
  constructor(fb: FormBuilder,
    private router: Router,
    private customerService:CustomerService) {
    this.formDateCustomer =fb.group({
      rut: [''],
      phone: [''],
      email: ['']
    })
   }

  ngOnInit() {
  }


  formIsOk(){
    if(this.formDateCustomer.value.rut==='' || this.formDateCustomer.value.phone==='' || this.formDateCustomer.value.email===''){
      return true;
    }
    else{
      return false;
    }
  }

  redirect() {
    
    this.customer.rut = (this.formDateCustomer.get('rut').value).toString();
    this.customer.celular = (this.formDateCustomer.get('phone').value).toString();
    this.customer.correo = (this.formDateCustomer.get('email').value).toString();
    this.customerService.setterCustomer(this.customer);
    this.router.navigate(['./renta']);
  }

}
