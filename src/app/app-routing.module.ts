import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorServicioComponent } from './modules/error-servicio/error-servicio.component';
import { DefaultComponent } from './layouts/default/default.component';
import { ComponentPrincipalTestComponent } from './modules/component-principal-test/component-principal-test.component';
import { ComponentOneComponent } from './modules/component-principal-test/component-one/component-one.component';
import { ComponentSecondComponent } from './modules/component-principal-test/component-second/component-second.component';


const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthGuard],
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: ComponentPrincipalTestComponent
      },
      {
        path: 'renta',
        component: ComponentSecondComponent
      },
      {
        path: 'error-servicio',
        component: ErrorServicioComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
