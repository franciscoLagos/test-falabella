import { Injectable } from '@angular/core';


import { LoaderInterface } from './../interfaces/loader.interface';
// tslint:disable-next-line: import-blacklist
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable()
export class LoaderService {
  private loaderSubject = new Subject<LoaderInterface>();
  loader = this.loaderSubject.asObservable();

  constructor() {}

  show() {
    this.loaderSubject.next(<LoaderInterface>{ visible: true });
  }

  hide() {
    this.loaderSubject.next(<LoaderInterface>{ visible: false });
  }
}
